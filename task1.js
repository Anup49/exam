const fs = require("fs");

try {
  const jsonString = fs.readFileSync("./input2.json");
  const data = JSON.parse(jsonString);
  const revValue = data.names;

  const newValue = revValue.map((x) => x.split("").reverse().join(""));

  // generating random string
  const ran = Math.random.toString(36).substring(2, 7);

  const newArr = newValue.map((x) => x.concat(ran, "@gmail.com"));
  console.log(newArr);
  fs.writeFile("./output2.json", JSON.stringify(newArr, null, 2), (err) => {
    if (err) {
      console.log(err);
    } else {
      console.log("File created successfully");
    }
  });
} catch (err) {
  console.log(err);
}
